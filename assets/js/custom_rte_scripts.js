


	$('.grid-rte').wysihat({
	    'buttons': ['bold', 'italic', 'underline',]
	});
	$('textarea').wysihat('undo'); // undo stack


	WysiHat.addButton("ytLink", {
    'label': EE.rte.ytLink.add,
    handler: function()
    {
        if (this.Selection.toString() == '')
        {
            alert('you must select text');
            return;
        }

        this.make('link', 'http://youtube.com');
    }
	});
