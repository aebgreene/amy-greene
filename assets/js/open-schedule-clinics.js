$(function() {
    $('#PID').change(function() {
        $('.error').html('');
        var thisID = $(this).val();
        $('#visitTypes').removeClass('hidden');
        $('#visitTypes').css('display', 'block');
        $('#VT').html('<option value="">-select-</option>');
        if ($('#visitTypes').hasClass('Visit')) {
            // do nothing
        } else if ($('#visitTypes').hasClass('Location')) {
         
            var jsToMoveLength = $('.jsToMove option').length;
            for (i = 0; i < jsToMoveLength; i++) {
                $('.jsToMove .-dept' + thisID).appendTo('#VT');
            }
            console.log('yesssss');
        } else {
           
            var jsToMoveLength = $('.jsToMove option').length;
            for (i = 0; i < jsToMoveLength; i++) {
                $('.jsToMove .-pid' + thisID).appendTo('#VT');
            }
        }
        
    });
})

// set global vars
var newstr = 0;

(function() {
    $("#tabs").tabs();
});

$('#form1').submit(function() {
    return false;
})

function showDiv() {
    document.getElementById("scheduleContainer").style.display = 'block';
}

function getValue(type) {
    var newstr;
    var getValueType = type;
    var error = 0;
    $('.error').html('&nbsp;');
    var message = [];
    var providerID = $("#PID option:selected").val();
    var visitID = $("#VT option:selected").val();
    var deptID = $("#VT option:selected").attr('dept_id');
    if (getValueType == "Visit Type" || getValueType == "Location") {
        // do nothing
    } else if (getValueType == 'Provider') {
        if (providerID == "" || visitID == "") {
            var errormsg = "<li>You must select a provider and a visit type.</li>";
            error = 1;
        } else {
            error = 0;
        }

        if (deptID == "") {
            deptID = '';
        }
        if (error == 1) {
            $('.error').html(errormsg);
            return false;
        } else {
            $('.error').html('&nbsp;');
        }
    }

 var newstr = 'https://www.mybellin.org/mychart/SignupAndSchedule/EmbeddedSchedule?id=' + providerID + ',' + deptID + '&vt=' + visitID;
    setTimeout(function() {
        $('#openSchedulingFrame').attr('src', newstr);
        showDiv();
    }, 1000);

}

var EWC = new EmbeddedWidgetController({
    // Replace with the hostname of your Open Scheduling site
    'hostname': 'https://bellin.org',
    // Must equal media query in EpicWP.css + any left/right margin of the host page. Should also change in EmbeddedWidget.css
    'matchMediaString': '(max-width: 747px)',
    //Show a button on top of the widget that lets the user see the slots in fullscreen.
    'showToggleBtn': true
});

