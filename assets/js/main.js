
$(document).ready(function() {

    //remove last child from elements with this class 
    $('.remove-last').children().last().remove();

    // MASTER SLIDER
    var slider = new MasterSlider();
    
    slider.setup('masterslider' , {
        layout:"fillwidth",
        width:1140,    // slider standard width
        height:385,   // slider standard height
        space:0,
        swipe:false,
        instantStartLayers:true,
        view:"mask",
        layersMode: "center",
        dataresize: true,
        autoplay: true     
        // more slider options goes here...
    });
    // adds Arrows navigation control to the slider.
    slider.control('arrows');


    function captionwidth() {
        var imgwidth = $('.captionate').width() - 40;
        $('.captionate .caption').width(imgwidth);
    }
    //fire on page load
    captionwidth();
    // fire on window resize
    $(window).on('resize', captionwidth);


    //demo 2 with multiple open accordions
    $('.accordion-trigger').accordify({
        singleOpen: false
    });


    $('.has-children ul').addClass("sub-menu");
    $('.sub-menu').wrap("<div class='plusMinus'></div>");


    $(".plusMinus").prepend("<span></span>");

    $("#menu-btn").click(function() {
        $(".menu-btn").toggleClass("open");
        $(".main-menu").toggleClass("open");
        $(".menu").toggleClass("open");
        $('.has-children ul').addClass("sub-menu");

    });


    $('.menu-btn').click(function() {
        $('.responsive-menu').toggleClass('expand')
    })

    $(".plusMinus span").click(function() {
        $(this).toggleClass("open");
        $(this).siblings().toggleClass("open");
    });

    $(".sub-menu").toggleClass(function() {
        if ($(this).is(".open")) {
            return "expand";
        }
    });

    

        
});


